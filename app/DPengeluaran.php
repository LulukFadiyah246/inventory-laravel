<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DPengeluaran extends Model
{
    protected $table 	  = 'dpengeluaran';
	protected $primaryKey = 'id_dpengeluaran';
}
