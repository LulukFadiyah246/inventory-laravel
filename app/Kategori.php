<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table 	  = 'kategori';
	protected $primaryKey = 'id_kategori';
	
	public function bahan_baku()
	{
		return $this->hasMany('App\BahanBaku', 'id_kategori');
	}
}
