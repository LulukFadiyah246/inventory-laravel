<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemakaian extends Model
{
	protected $table 	  = 'pemakaian';
	protected $primaryKey = 'id_pemakaian';
}
