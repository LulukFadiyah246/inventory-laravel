<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisPengeluaran extends Model
{
    protected $table  	  = 'jenis_pengeluaran';
	protected $primaryKey = 'id_jenis_pengeluaran';

	public function pengeluaran()
	{
    	return $this->hasMany('App\Pengeluaran', 'id_supplier');
    }
}
