<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table  	  = 'karyawan';
	protected $primaryKey = 'id_karyawan';

	public function pemakaian()
	{
    	return $this->hasMany('App\Pemakaian', 'id_supplier');
    }
}
