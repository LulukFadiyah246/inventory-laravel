<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DPemakaian extends Model
{
	protected $table 	  = 'dpemakaian';
	protected $primaryKey = 'id_dpemakaian';
}
