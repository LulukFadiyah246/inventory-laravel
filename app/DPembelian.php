<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DPembelian extends Model
{
    protected $table 	  = 'dpembelian';
	protected $primaryKey = 'id_dpembelian';
}
