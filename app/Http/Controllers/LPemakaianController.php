<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\Pemakaian;
use App\DPemakaian;
use App\BahanBaku;
use App\Karyawan;
use PDF;

class LPemakaianController extends Controller
{
  public function index()
  {
    $awal 	= date('Y-m-d', mktime(0,0,0, date('m'), 1, date('Y')));
    $akhir 	= date('Y-m-d');
    return view('lpemakaian.index', compact('awal', 'akhir')); 
  }

  protected function getData($awal, $akhir)
  {
    $no 	  = 0;
    $data 	= array();
    while(strtotime($awal) <= strtotime($akhir))
    {
      $tanggal   = $awal;
      $awal 	   = date('Y-m-d', strtotime("+1 day", strtotime($awal)));
      $total 	   = 0;

      $pemakaian = Pemakaian::leftJoin('karyawan', 'karyawan.id_karyawan', '=', 'pemakaian.id_karyawan')
      ->orderBy('pemakaian.id_pemakaian', 'desc')
      ->get();
      $total_pemakaian = Pemakaian::sum('total_harga');
      $total 			    += $total_pemakaian;
      $no 	    = 0;
      $data 	  = array();
			foreach($pemakaian as $list)
			{
       	$no ++;
       	$row 	  = array();
       	$row[] 	= $no;
       	$row[] 	= tanggal_indonesia(substr($list->created_at, 0, 10), false);
       	$row[] 	= $list->nama;
       	$row[] 	= $list->total_item;
       	$row[] 	= "Rp. ".format_uang($list->total_harga);
       	$data[] = $row;
      }
    }
    $data[] = array("", "", "", "Total Pemakaian", "Rp. ".format_uang($total_pemakaian),"");
    return $data;
  }

  public function listData($awal, $akhir)
  {   
    $data 	= $this->getData($awal, $akhir);

    $output = array("data" => $data);
    return response()->json($output);
  }

  public function refresh(Request $request)
  {
    $awal 	= $request['awal'];
    $akhir 	= $request['akhir'];
    return view('lpemakaian.index', compact('awal', 'akhir')); 
  }

  public function exportPDF($awal, $akhir)
  {
    $tanggal_awal 	= $awal;
    $tanggal_akhir 	= $akhir;
    $data 			    = $this->getData($awal, $akhir);

    $pdf 			      = PDF::loadView('lpemakaian.pdf', compact('tanggal_awal', 'tanggal_akhir', 'data'));
    $pdf->setPaper('a4', 'landscape');
     
    return $pdf->stream();
  }
}
