<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Alert;
use App\Setting;
use App\Kategori;
use App\BahanBaku;
use App\Supplier;
use App\Karyawan;
use App\Pemakaian;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Alert::success('Selamat Datang', 'Login Berhasil');
        $setting = Setting::find(1);

        $awal    = date('Y-m-d', mktime(0,0,0, date('m'), 1, date('Y')));
        $akhir   = date('Y-m-d');

        $tanggal         = $awal;
        $data_tanggal    = array();
        $data_penggunaan = array();

        while(strtotime($tanggal) <= strtotime($akhir))
        { 
            $data_tanggal[]        = (int)substr($tanggal,8,2);
            
            $penggunaan            = Pemakaian::where('created_at', 'LIKE', "$tanggal%")->sum('total_harga');
            $data_penggunaan[]     = (int) $penggunaan;

            $tanggal               = date('Y-m-d', strtotime("+1 day", strtotime($tanggal)));
        }
        
        $kategori    = Kategori::count();
        $bahan_baku  = BahanBaku::count();
        $supplier    = Supplier::count();
        $karyawan    = Karyawan::count();

        if(Auth::user()->level == 1) return view('home.admin', compact('kategori', 'bahan_baku', 'supplier', 'karyawan', 'awal', 'akhir', 'data_penggunaan', 'data_tanggal'));
        else return view('home.umum', compact('laporan', 'lpembelian', 'lpemakaian', 'lpengeluaran'));
    }
}
