<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Auth;
use PDF;
use App\Pemakaian;
use App\DPemakaian;
use App\BahanBaku;
use App\Karyawan;

class DPemakaianController extends Controller
{
  //Fungsi Utama
	public function index()
	{
    $bahan_baku  = BahanBaku::all();
    $idpemakaian = session('idpemakaian');
    $karyawan    = Karyawan::find(session('idkaryawan'));
    return view('dpemakaian.index', compact('bahan_baku', 'idpemakaian', 'karyawan'));
  }

  //Fungsi yang akan dipanggil setelah index dgn parameter id yang akan dipanggil datanya
  public function listData($id)
  {
    $detail 	    = DPemakaian::leftJoin('bahan_baku', 'bahan_baku.kode_bahan_baku', '=', 'dpemakaian.kode_bahan_baku')
    ->where('id_pemakaian', '=', $id)
    ->get();
    $no 		      = 0;
    $data 		    = array();
    $total        = 0;
    $total_item   = 0;
    foreach($detail as $list)
    {
      $no ++;
      $row 	  = array();
      $row[] 	= $no;
      $row[] 	= $list->kode_bahan_baku;
      $row[] 	= $list->nama_bahan_baku;
      $row[] 	= "Rp. ".format_uang($list->harga_beli);
      $row[] 	= "<input type='number' class='form-control' name='jumlah_$list->id_dpemakaian' value='$list->jumlah' min='1' max='$list->stok' onChange='changeCount($list->id_dpemakaian)'>";
      $row[] 	= "Rp. ".format_uang($list->harga_beli * $list->jumlah);
      $row[] 	= '<a onclick="deleteItem('.$list->id_dpemakaian.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
      //Baris untuk menyimpan perhitungan
      $data[] = $row;

      $total        += $list->harga_beli * $list->jumlah;
      $total_item   += $list->jumlah;
    }

    //Membuat hidden data untuk keperluan perhitungan total dan total item
    $data[] = array("<span class='hide total'>$total</span><span class='hide totalitem'>$total_item</span>", "", "", "", "", "", "");
    
    $output = array("data" => $data);
    return response()->json($output);
  }

  public function store(Request $request)
  {
    $bahan_baku              = BahanBaku::where('kode_bahan_baku', '=', $request['kode'])->first();

    $detail 				         = new DPemakaian;
    $detail->id_pemakaian 	 = $request['idpemakaian'];
    $detail->kode_bahan_baku = $request['kode'];
    $detail->harga_beli 	   = $bahan_baku->harga_beli;
    $detail->jumlah 		     = 1;
    $detail->sub_total 		   = $bahan_baku->harga_beli;
    $detail->save();
  }

  public function update(Request $request, $id)
  {
    $nama_input        = "jumlah_".$id;
    $detail 	         = DPemakaian::find($id);
    $detail->jumlah    = $request[$nama_input];
    $detail->sub_total = $detail->harga_beli * $request[$nama_input];
    $detail->update();
  }

  public function destroy($id)
  {
    $detail = DPemakaian::find($id);
    $detail->delete();
  }
   
  public function loadForm($total)
  {
    $data = array
    (
      "totalrp"        => format_uang($total),
      "terbilang"      => ucwords(terbilang($total))." Rupiah"
    );
    return response()->json($data);
  }
}