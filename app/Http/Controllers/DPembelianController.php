<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\Pembelian;
use App\DPembelian; 
use App\Supplier;
use App\BahanBaku;

class DPembelianController extends Controller
{
  //Fungsi utama
	public function index()
	{
    $bahan_baku  = BahanBaku::all();
    $idpembelian = session('idpembelian');
    $supplier 	 = Supplier::find(session('idsupplier'));
    return view('dpembelian.index', compact('bahan_baku', 'idpembelian', 'supplier'));
  }

  //Fungsi yang akan dipanggil setelah index dgn parameter id yang akan dipanggil datanya
  public function listData($id)
  {
    $detail     = DPembelian::leftJoin('bahan_baku', 'bahan_baku.kode_bahan_baku', '=', 'dpembelian.kode_bahan_baku')
    ->where('id_pembelian', '=', $id)
    ->get();
    $no 		    = 0;
    $data 		  = array();
    $total 		  = 0;
    $total_item = 0;
    foreach($detail as $list)
    {
      $no ++;
      $row 	  = array();
      $row[] 	= $no;
      $row[] 	= $list->kode_bahan_baku;
      $row[] 	= $list->nama_bahan_baku;
      $row[] 	= "Rp. ".format_uang($list->harga_beli);
      $row[] 	= "<input type='number' class='form-control' name='jumlah_$list->id_dpembelian' value='$list->jumlah' onChange='changeCount($list->id_dpembelian)'>";
      $row[] 	= "Rp. ".format_uang($list->harga_beli * $list->jumlah);
      $row[] 	= '<a onclick="deleteItem('.$list->id_dpembelian.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>';
      //Baris untuk menyimpan perhitungan
      $data[] = $row;

      $total 		  += $list->harga_beli * $list->jumlah;
      $total_item += $list->jumlah;
    }
    
    //Membuat hidden data untuk keperluan perhitungan total dan total item
    $data[] = array("<span class='hide total'>$total</span><span class='hide totalitem'>$total_item</span>", "", "", "", "", "", "");
    
    $output = array("data" => $data);
    return response()->json($output);
  }

  public function store(Request $request)
  {
    $bahan_baku                  = BahanBaku::where('kode_bahan_baku', '=', $request['kode'])->first();
    $detail                      = new DPembelian;
    $detail->id_pembelian        = $request['idpembelian'];
    $detail->kode_bahan_baku     = $request['kode'];
    $detail->harga_beli          = $bahan_baku->harga_beli;
    $detail->jumlah              = 1;
    $detail->sub_total           = $bahan_baku->harga_beli;
    $detail->save();
  }

  public function update(Request $request, $id)
  {
    $nama_input        = "jumlah_".$id;
    $detail            = DPembelian::find($id);
    $detail->jumlah    = $request[$nama_input];
    $detail->sub_total = $detail->harga_beli * $request[$nama_input];
    $detail->update();
  }

  public function destroy($id)
  {
    $detail = DPembelian::find($id);
    $detail->delete();
  }

  //Perhitungan Total dengan format uang
  public function loadForm($diskon, $total)
  {
    $bayar = $total - ($diskon / 100 * $total);
    $data  = array
    (
      "totalrp"   => format_uang($total),
      "bayar"     => $bayar,
      "bayarrp"   => format_uang($bayar),
      "terbilang" => ucwords(terbilang($bayar))." Rupiah"
    );
    return response()->json($data);
  }
}