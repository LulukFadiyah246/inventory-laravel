<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\JenisPengeluaran;

class JenisPengeluaranController extends Controller
{
    //Fungsi utama
    public function index()
    {
        return view('jenis_pengeluaran.index'); 
    }

    //Fungsi yang akan dipanggil setelah index
    public function listData()
    {
        $jenis_pengeluaran   = JenisPengeluaran::orderBy('id_jenis_pengeluaran', 'desc')->get();
        $no         = 0;
        $data       = array();
        foreach($jenis_pengeluaran as $list)
        {
            $no ++;
            $row    = array();
            $row[]  = $no;
            $row[]  = $list->kode_jenis_pengeluaran;
            $row[]  = $list->nama;
            $row[]  = '<div class="btn-group">
                       	<a onclick="editForm('.$list->id_jenis_pengeluaran.')" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                       	<a onclick="deleteData('.$list->id_jenis_pengeluaran.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                       </div>';
            $data[] = $row;
        }
        $output     = array("data" => $data);
        return response()->json($output);
    }

    public function store(Request $request)
    {
        $jml = JenisPengeluaran::where('kode_jenis_pengeluaran', '=', $request['kode'])->count();
        if($jml < 1)
        {
            $jenis_pengeluaran                   		  = new JenisPengeluaran;
            $jenis_pengeluaran->kode_jenis_pengeluaran    = $request['kode'];
            $jenis_pengeluaran->nama             		  = $request['nama'];
            $jenis_pengeluaran->save();
            echo json_encode(array('msg'=>'success'));
        }
        else
        {
            echo json_encode(array('msg'=>'error'));
        }
    }

    public function edit($id)
    {
        $jenis_pengeluaran = JenisPengeluaran::find($id);
        echo json_encode($jenis_pengeluaran);
    }

    public function update(Request $request, $id)
    {
        $jml = JenisPengeluaran::where('nama', '=', $request['nama'])->count();

        if($jml < 1)
        {
            $jenis_pengeluaran                   = JenisPengeluaran::find($id);
            $jenis_pengeluaran->nama             = $request['nama'];
            $jenis_pengeluaran->update();
            echo json_encode(array('msg'=>'success'));
        }
        else
        {
            echo json_encode(array('msg'=>'error'));
        }
    }

    public function destroy($id)
    {
        $jenis_pengeluaran = JenisPengeluaran::find($id);
        $jenis_pengeluaran->delete();
    }
}
