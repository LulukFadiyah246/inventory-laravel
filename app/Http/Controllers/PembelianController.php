<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\Pembelian;
use App\DPembelian;
use App\Supplier;
use App\BahanBaku;

class PembelianController extends Controller
{
  //Fungsi utama
	public function index()
  {
    $supplier  = Supplier::all();
    //Mengirirmkan data Supplier
    return view('pembelian.index', compact('supplier')); 
  }

  //Fungsi yang akan dipanggil setelah index
  public function listData()
  {
    $pembelian = Pembelian::leftJoin('supplier', 'supplier.id_supplier', '=', 'pembelian.id_supplier')
    ->orderBy('pembelian.id_pembelian', 'desc')
    ->get();
    $no        = 0;
    $data      = array();
    foreach($pembelian as $list)
    {
      $no ++;
      $row 	  = array();
      $row[] 	= $no;
      $row[] 	= tanggal_indonesia(substr($list->created_at, 0, 10), false);
      $row[] 	= $list->nama;
      $row[] 	= $list->total_item;
      $row[] 	= "Rp. ".format_uang($list->total_harga);
      $row[] 	= $list->diskon."%";
      $row[] 	= "Rp. ".format_uang($list->bayar);
      $row[] 	= '<div class="btn-group">
               	  <a onclick="showDetail('.$list->id_pembelian.')" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
               	  <a onclick="deleteData('.$list->id_pembelian.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
              	 </div>';
      $data[] = $row;
    }
    $output 	= array("data" => $data);
    return response()->json($output);
  }

  public function show($id)
  {
    $detail = DPembelian::leftJoin('bahan_baku', 'bahan_baku.kode_bahan_baku', '=', 'dpembelian.kode_bahan_baku')
    ->where('id_pembelian', '=', $id)
    ->get();
    $no 	  = 0;
    $data 	= array();
    foreach($detail as $list)
    {
      $no ++;
      $row 	  = array();
      $row[] 	= $no;
      $row[] 	= $list->kode_bahan_baku;
      $row[] 	= $list->nama_bahan_baku;
      $row[] 	= "Rp. ".format_uang($list->harga_beli);
      $row[] 	= $list->jumlah;
      $row[] 	= "Rp. ".format_uang($list->harga_beli * $list->jumlah);
      $data[] = $row;
    }
    $output 	= array("data" => $data);
    return response()->json($output);
  }

  //Menyimpan data pembelian baru dengan kirim id supplier saja
  public function create($id)
  {
    $pembelian 				      = new Pembelian;
    $pembelian->id_supplier = $id;     
    $pembelian->total_item 	= 0;     
    $pembelian->total_harga = 0;     
    $pembelian->diskon 		  = 0;     
    $pembelian->bayar 		  = 0;     
    $pembelian->save();

    session(['idpembelian' 	=> $pembelian->id_pembelian]);
    session(['idsupplier' 	=> $id]);

    return Redirect::route('dpembelian.index');      
  }

  public function store(Request $request)
  {
    //Mengupdate data pembelian
    $pembelian 				      = Pembelian::find($request['idpembelian']);
    $pembelian->total_item 	= $request['totalitem'];
    $pembelian->total_harga = $request['total'];
    $pembelian->diskon 		  = $request['diskon'];
    $pembelian->bayar 		  = $request['bayar'];
    $pembelian->update();

    //Mengupdate data stok Bahan Baku
    $detail                 = DPembelian::where('id_pembelian', '=', $request['idpembelian'])->get();
    foreach($detail as $data)
    {
      $bahan_baku 	     = BahanBaku::where('kode_bahan_baku', '=', $data->kode_bahan_baku)->first();
      $bahan_baku->stok += $data->jumlah;
      $bahan_baku->update();
    }
    return Redirect::route('pembelian.index');
  }
   
  public function destroy($id)
  {
    //Menghapus data pembelian
    $pembelian 	= Pembelian::find($id);
    $pembelian->delete();

    //Menghapus data detail pembelian yang otomatis update stok Bahan Baku
    $detail 	  = DPembelian::where('id_pembelian', '=', $id)->get();
    foreach($detail as $data)
    {
      $bahan_baku 	     = BahanBaku::where('kode_bahan_baku', '=', $data->kode_bahan_baku)->first();
      $bahan_baku->stok -= $data->jumlah;
      $bahan_baku->update();
      $data->delete();
    }
  }
}
