<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\Pengeluaran;
use App\DPengeluaran;
use App\BahanBaku;
use App\JenisPengeluaran;
use PDF;

class LPengeluaranController extends Controller
{
  public function index()
  {
    $awal 	= date('Y-m-d', mktime(0,0,0, date('m'), 1, date('Y')));
    $akhir 	= date('Y-m-d');
    return view('lpengeluaran.index', compact('awal', 'akhir')); 
  }

  protected function getData($awal, $akhir)
  {
    $no 	  = 0;
    $data 	= array();
    while(strtotime($awal) <= strtotime($akhir))
    {
      $tanggal     = $awal;
      $awal 	     = date('Y-m-d', strtotime("+1 day", strtotime($awal)));
      $total 	     = 0;

      $pengeluaran = Pengeluaran::leftJoin('jenis_pengeluaran', 'jenis_pengeluaran.id_jenis_pengeluaran', '=', 'pengeluaran.id_jenis_pengeluaran')
      ->orderBy('pengeluaran.id_pengeluaran', 'desc')
      ->get();
      $total_pengeluaran = Pengeluaran::sum('total_harga');
      $total 			      += $total_pengeluaran;
      $no 	    = 0;
      $data 	  = array();
			foreach($pengeluaran as $list)
			{
       	$no ++;
       	$row 	  = array();
       	$row[] 	= $no;
       	$row[] 	= tanggal_indonesia(substr($list->created_at, 0, 10), false);
       	$row[] 	= $list->nama;
       	$row[] 	= $list->total_item;
       	$row[] 	= "Rp. ".format_uang($list->total_harga);
       	$data[] = $row;
      }
    }
    $data[]     = array("", "", "", "Total Pengeluaran", "Rp. ".format_uang($total_pengeluaran),"");
    return $data;
  }

  public function listData($awal, $akhir)
  {   
    $data 	= $this->getData($awal, $akhir);

    $output = array("data" => $data);
    return response()->json($output);
  }

  public function refresh(Request $request)
  {
    $awal 	= $request['awal'];
    $akhir 	= $request['akhir'];
    return view('lpengeluaran.index', compact('awal', 'akhir')); 
  }

  public function exportPDF($awal, $akhir)
  {
    $tanggal_awal 	= $awal;
    $tanggal_akhir 	= $akhir;
    $data 			    = $this->getData($awal, $akhir);

    $pdf 			      = PDF::loadView('lpengeluaran.pdf', compact('tanggal_awal', 'tanggal_akhir', 'data'));
    $pdf->setPaper('a4', 'landscape');
     
    return $pdf->stream();
  }
}
