<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BahanBaku;
use App\Kategori;
use Datatables;
use PDF;

class BahanBakuController extends Controller
{
  //Fungsi utama
  public function index()
  {
    $kategori   = Kategori::all();  
    //mengirimkan data kategori    
    return view('bahan_baku.index', compact('kategori'));
  }

  public function listData()
  {
    $bahan_baku = BahanBaku::leftJoin('kategori', 'kategori.id_kategori', '=', 'bahan_baku.id_kategori')
    ->orderBy('bahan_baku.id_bahan_baku', 'desc')
    ->get();
    $no         = 0;
    $data       = array();
    foreach($bahan_baku as $list)
    {
      $no ++;
      $row    = array();
      $row[]  = "<input type='checkbox' name='id[]'' value='".$list->id_bahan_baku."'>";
      $row[]  = $no;
      $row[]  = $list->kode_bahan_baku;
      $row[]  = $list->nama_bahan_baku;
      $row[]  = $list->nama_kategori;
      $row[]  = $list->satuan;
      $row[]  = "Rp. ".format_uang($list->harga_beli);
      $row[]  = $list->diskon."%";
      $row[]  = $list->stok;
      $row[]  = "<div class='btn-group'>
                  <a onclick='editForm(".$list->id_bahan_baku.")' class='btn btn-primary btn-sm'><i class='fa fa-pencil'></i></a>
                  <a onclick='deleteData(".$list->id_bahan_baku.")' class='btn btn-danger btn-sm'><i class='fa fa-trash'></i></a>
                 </div>";
      $data[] = $row;
    }
    //Menggunakan Server Side Processing dari Plugin DataTable
    return Datatables::of($data)->escapeColumns([])->make(true);
  }

  public function store(Request $request)
  {
    //Lakukan cek apakah kode sudah digunakan
    $jml = BahanBaku::where('kode_bahan_baku', '=', $request['kode'])->count();

    if($jml < 1)
    {
      $bahan_baku                  = new BahanBaku;
      $bahan_baku->kode_bahan_baku = $request['kode'];
      $bahan_baku->nama_bahan_baku = $request['nama'];
      $bahan_baku->id_kategori     = $request['kategori'];
      $bahan_baku->satuan          = $request['satuan'];
      $bahan_baku->harga_beli      = $request['harga_beli'];
      $bahan_baku->diskon          = $request['diskon'];
      $bahan_baku->stok            = $request['stok'];
      $bahan_baku->save();
      echo json_encode(array('msg' =>'success'));
    }
    else
    {
      echo json_encode(array('msg' =>'error'));
    }
  }

  public function edit($id)
  {
    $bahan_baku = BahanBaku::find($id);
    echo json_encode($bahan_baku);
  }

  public function update(Request $request, $id)
  {
    $bahan_baku                  = BahanBaku::find($id);
    $bahan_baku->nama_bahan_baku = $request['nama'];
    $bahan_baku->id_kategori     = $request['kategori'];
    $bahan_baku->satuan          = $request['satuan'];
    $bahan_baku->harga_beli      = $request['harga_beli'];
    $bahan_baku->diskon          = $request['diskon'];
    $bahan_baku->stok            = $request['stok'];
    $bahan_baku->update();
    echo json_encode(array('msg' =>'success'));
  }

  public function destroy($id)
  {
    $bahan_baku   = BahanBaku::find($id);
    $bahan_baku->delete();
  }

  //Fungsi sama seperti destroy, hanya melakukan perulangan sesuai jumlah CheckBox yang dicentang
  public function deleteSelected(Request $request)
  {
    foreach($request['id'] as $id)
    {
      $bahan_baku = BahanBaku::find($id);
      $bahan_baku->delete();
    }
  }

  //Mencetak Laporan
  public function show(Request $request, $id)
  {
    $bahan_baku   = BahanBaku::all();
    $pdf          = PDF::loadView('bahan_baku/lbahanbaku', compact('bahan_baku'));
    $pdf->setPaper('a4', 'landscape');
    return $pdf->stream('lbahanbaku.pdf');
  }
}