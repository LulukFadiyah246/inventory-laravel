<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Karyawan;

class KaryawanController extends Controller
{
    //Fungsi Utama
    public function index()
    {
        return view('karyawan.index'); 
    }

    //Fungsi yang akan dipanggil setelah index
    public function listData()
    {
        $karyawan   = Karyawan::orderBy('id_karyawan', 'desc')->get();
        $no         = 0;
        $data       = array();
        foreach($karyawan as $list)
        {
            $no ++;
            $row    = array();
            $row[]  = "<input type='checkbox' name='id[]'' value='".$list->id_karyawan."'>";
            $row[]  = $no;
            $row[]  = $list->kode_karyawan;
            $row[]  = $list->nama;
            $row[]  = $list->alamat;
            $row[]  = $list->telpon;
            $row[]  = '<div class="btn-group">
                       	<a onclick="editForm('.$list->id_karyawan.')" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                       	<a onclick="deleteData('.$list->id_karyawan.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                       </div>';
            $data[] = $row;
        }
        $output     = array("data" => $data);
        return response()->json($output);
    }

    public function store(Request $request)
    {
        $jml = Karyawan::where('kode_karyawan', '=', $request['kode'])->count();
        
        if($jml < 1)
        {
            $karyawan                   = new Karyawan;
            $karyawan->kode_karyawan    = $request['kode'];
            $karyawan->nama             = $request['nama'];
            $karyawan->alamat           = $request['alamat'];
            $karyawan->telpon           = $request['telpon'];
            $karyawan->save();
            echo json_encode(array('msg'=>'success'));
        }
        else
        {
            echo json_encode(array('msg'=>'error'));
        }
    }

    public function edit($id)
    {
        $karyawan = Karyawan::find($id);
        echo json_encode($karyawan);
    }

    public function update(Request $request, $id)
    {
        $karyawan                   = Karyawan::find($id);
        $karyawan->nama             = $request['nama'];
        $karyawan->alamat           = $request['alamat'];
        $karyawan->telpon           = $request['telpon'];
        $karyawan->update();
        echo json_encode(array('msg'=>'success'));
    }

    public function destroy($id)
    {
        $karyawan = Karyawan::find($id);
        $karyawan->delete();
    }
} 