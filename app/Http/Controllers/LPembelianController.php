<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use App\Pembelian;
use App\DPembelian;
use App\Supplier;
use App\BahanBaku;
use PDF; 

class LPembelianController extends Controller
{
	public function index()
  {
    $awal 	= date('Y-m-d', mktime(0,0,0, date('m'), 1, date('Y')));
    $akhir 	= date('Y-m-d');
    return view('lpembelian.index', compact('awal', 'akhir')); 
  }

  protected function getData($awal, $akhir)
  {
    $no 	       = 0;
    $data 	     = array();
    while(strtotime($awal) <= strtotime($akhir))
    {
      $tanggal   = $awal;
      $awal 	   = date('Y-m-d', strtotime("+1 day", strtotime($awal)));
      $total 	   = 0;      

     	$pembelian = Pembelian::leftJoin('supplier', 'supplier.id_supplier', '=', 'pembelian.id_supplier')
      ->orderBy('pembelian.id_pembelian', 'desc')
      ->get();
     	$total_pembelian = Pembelian::sum('bayar');
     	$total 		 	    += $total_pembelian;
     	$no 			= 0;
     	$data 		= array();
     	foreach($pembelian as $list)
     	{
       	$no ++;
       	$row 	  = array();
       	$row[] 	= $no;
       	$row[] 	= tanggal_indonesia(substr($list->created_at, 0, 10), false);
       	$row[] 	= $list->nama;
       	$row[] 	= $list->total_item;
       	$row[] 	= "Rp. ".format_uang($list->total_harga);
       	$row[] 	= $list->diskon."%";
       	$row[] 	= "Rp. ".format_uang($list->bayar);
       	$data[] = $row;
      }
    }
    $data[]     = array("", "", "", "", "", "Total Pembelian", "Rp. ".format_uang($total_pembelian),"");
    return $data;
  }

  public function listData($awal, $akhir)
  {   
    $data 	= $this->getData($awal, $akhir);

    $output = array("data" => $data);
    return response()->json($output);
  }

  public function refresh(Request $request)
  {
    $awal 	= $request['awal'];
    $akhir 	= $request['akhir'];
    return view('lpembelian.index', compact('awal', 'akhir')); 
  }

  public function exportPDF($awal, $akhir)
  {
    $tanggal_awal  = $awal;
    $tanggal_akhir = $akhir;
    $data 		     = $this->getData($awal, $akhir);

    $pdf 		       = PDF::loadView('lpembelian.pdf', compact('tanggal_awal', 'tanggal_akhir', 'data'));
    $pdf->setPaper('a4', 'potrait');
     
    return $pdf->stream();
  }
}
