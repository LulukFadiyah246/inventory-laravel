<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Alert;
use App\Pengeluaran;
use App\DPengeluaran; 
use App\BahanBaku;
use App\JenisPengeluaran;

class PengeluaranController extends Controller
{
  //Fungsi Utama
  public function index()
  {
    $jenis_pengeluaran  = JenisPengeluaran::all();
    //Mengirirmkan data Jenis Pengeluaran
    return view('pengeluaran.index', compact('jenis_pengeluaran')); 
  }

  //Fungsi yang akan dipanggil setelah index
  public function listData()
  {
    $pengeluaran = Pengeluaran::leftJoin('jenis_pengeluaran', 'jenis_pengeluaran.id_jenis_pengeluaran', '=', 'pengeluaran.id_jenis_pengeluaran')
    ->orderBy('pengeluaran.id_pengeluaran', 'desc')
    ->get();
    $no       = 0;
    $data     = array();
    foreach($pengeluaran as $list)
    {
      $no ++;
      $row    = array();
      $row[]  = $no;
      $row[]  = tanggal_indonesia(substr($list->created_at, 0, 10), false);
      $row[]  = $list->nama;
      $row[]  = $list->total_item;
      $row[]  = "Rp. ".format_uang($list->total_harga);
      $row[]  = '<div class="btn-group">
                  <a onclick="showDetail('.$list->id_pengeluaran.')" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                  <a onclick="deleteData('.$list->id_pengeluaran.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                 </div>';
      $data[] = $row;
    }
    $output   = array("data" => $data);
    return response()->json($output);
  }

  public function show($id)
  {
    $detail   = DPengeluaran::leftJoin('bahan_baku', 'bahan_baku.kode_bahan_baku', '=', 'dpengeluaran.kode_bahan_baku')
    ->where('id_pengeluaran', '=', $id)
    ->get();  
    $no       = 0;
    $data     = array();
    foreach($detail as $list)
    {
      $no ++;
      $row    = array(); 
      $row[]  = $no;
      $row[]  = $list->kode_bahan_baku;
      $row[]  = $list->nama_bahan_baku;
      $row[]  = "Rp. ".format_uang($list->harga_beli);
      $row[]  = $list->jumlah;
      $row[]  = "Rp. ".format_uang($list->sub_total);
      $data[] = $row;
    }
    $output   = array("data" => $data);
    return response()->json($output);
  }

  //Menyimpan data pengeluaran baru dengan kirim id jenis_pengeluaran saja
  public function create($id)
  {
    $pengeluaran                         = new Pengeluaran;
    $pengeluaran->id_jenis_pengeluaran   = $id;     
    $pengeluaran->total_item             = 0;     
    $pengeluaran->total_harga            = 0;     
    $pengeluaran->save();

    session(['idpengeluaran'             => $pengeluaran->id_pengeluaran]);
    session(['idjenispengeluaran'        => $id]);

    return Redirect::route('dpengeluaran.index');      
  }

  public function store(Request $request)
  {
    //Mengupdate data pengeluaran
    $pengeluaran              = Pengeluaran::find($request['idpengeluaran']);
    $pengeluaran->total_item  = $request['totalitem'];
    $pengeluaran->total_harga = $request['total'];
    $detail                   = DPengeluaran::where('id_pengeluaran', '=', $request['idpengeluaran'])->get();
    foreach($detail as $data) 
    {
      $bahan_baku               = BahanBaku::where('kode_bahan_baku', '=', $data->kode_bahan_baku)->first();
      
      if($request['totalitem']  < $bahan_baku->stok)
      {
        $pengeluaran->update();
        //Mengupdate data stok Bahan Baku
        $detail                   = DPengeluaran::where('id_pengeluaran', '=', $request['idpengeluaran'])->get();
        foreach($detail as $data)
        {
          $bahan_baku        = BahanBaku::where('kode_bahan_baku', '=', $data->kode_bahan_baku)->first();
          $bahan_baku->stok -= $data->jumlah;
          $bahan_baku->update();
        }
        Alert::success("Data Pengeluaran Tersimpan!", "", "success");
        return Redirect::route('pengeluaran.index');
      }
      else
      {
        Alert::error("Stok tidak cukup","","warning");
        return Redirect::route('dpengeluaran.index');
      }
    }
  }
   
  public function destroy($id)
  {
    //Menghapus data pengeluaran
    $pengeluaran = Pengeluaran::find($id);
    $pengeluaran->delete();

    //Menghapus data detail pengeluaran yang otomatis update stok Bahan Baku
    $detail      = DPengeluaran::where('id_pengeluaran', '=', $id)->get();
    foreach($detail as $data)
    {
      $bahan_baku        = BahanBaku::where('kode_bahan_baku', '=', $data->kode_bahan_baku)->first();
      $bahan_baku->stok += $data->jumlah;
      $bahan_baku->update();
      $data->delete();
    }
  }
}