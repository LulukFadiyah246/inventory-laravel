<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Alert;
use App\Pemakaian;
use App\DPemakaian;
use App\BahanBaku;
use App\Karyawan;

class PemakaianController extends Controller
{
  //Fungsi Utama
	public function index()
	{
    $karyawan  = Karyawan::all();
    //Mengirirmkan data Karyawan
    return view('pemakaian.index', compact('karyawan')); 
  }

  //Fungsi yang akan dipanggil setelah index
  public function listData()
  {
    $pemakaian = Pemakaian::leftJoin('karyawan', 'karyawan.id_karyawan', '=', 'pemakaian.id_karyawan')
    ->orderBy('pemakaian.id_pemakaian', 'desc')
    ->get();
    $no       = 0;
    $data     = array();
    foreach($pemakaian as $list)
    {
      $no ++;
      $row    = array();
      $row[]  = $no;
      $row[]  = tanggal_indonesia(substr($list->created_at, 0, 10), false);
      $row[]  = $list->nama;
      $row[]  = $list->total_item;
      $row[]  = "Rp. ".format_uang($list->total_harga);
      $row[]  = '<div class="btn-group">
                  <a onclick="showDetail('.$list->id_pemakaian.')" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                  <a onclick="deleteData('.$list->id_pemakaian.')" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                 </div>';
      $data[] = $row;
    }
    $output   = array("data" => $data);
    return response()->json($output);
  }

  public function show($id)
  {
    $detail = DPemakaian::leftJoin('bahan_baku', 'bahan_baku.kode_bahan_baku', '=', 'dpemakaian.kode_bahan_baku')
    ->where('id_pemakaian', '=', $id)
    ->get();
    $no     = 0;
    $data   = array();
    foreach($detail as $list)
    {
      $no ++;
      $row    = array();
      $row[]  = $no;
      $row[]  = $list->kode_bahan_baku;
      $row[]  = $list->nama_bahan_baku;
      $row[]  = "Rp. ".format_uang($list->harga_beli);
      $row[]  = $list->jumlah;
      $row[]  = "Rp. ".format_uang($list->harga_beli * $list->jumlah);
      $data[] = $row;
    }
    $output   = array("data"  => $data);
    return response()->json($output);
  }

  //Menyimpan data pemakaian baru dengan kirim id karyawan saja
  public function create($id)
  {
    $pemakaian                = new Pemakaian;
    $pemakaian->id_karyawan   = $id;     
    $pemakaian->total_item    = 0;     
    $pemakaian->total_harga   = 0;     
    $pemakaian->save();

    session(['idpemakaian'    => $pemakaian->id_pemakaian]);
    session(['idkaryawan'     => $id]);

    return Redirect::route('dpemakaian.index');      
  }
   
  public function store(Request $request)
  {
    //Mengupdate data pemakaian
    $pemakaian              = Pemakaian::find($request['idpemakaian']);
    $pemakaian->total_item  = $request['totalitem'];
    $pemakaian->total_harga = $request['total'];
    $detail                 = DPemakaian::where('id_pemakaian', '=', $request['idpemakaian'])->get();
    foreach($detail as $data)
    {
      $bahan_baku        = BahanBaku::where('kode_bahan_baku', '=', $data->kode_bahan_baku)->first();

      if($request['totalitem']  < $bahan_baku->stok)
      {
        $pemakaian->update();
        //Mengupdate data stok Bahan Baku
        $detail                   = DPemakaian::where('id_pemakaian', '=', $request['idpemakaian'])->get();
        foreach($detail as $data)
        {
          $bahan_baku        = BahanBaku::where('kode_bahan_baku', '=', $data->kode_bahan_baku)->first();
          $bahan_baku->stok -= $data->jumlah;
          $bahan_baku->update();
        }
        Alert::success("Data Pemakaian Tersimpan!", "", "success");
        return Redirect::route('pemakaian.index');
      }
      else
      {
        Alert::error("Stok tidak cukup","","warning");
        return Redirect::route('dpemakaian.index');
      }
    }
  }

  public function destroy($id)
  {
    //Menghapus data pemakaian
    $pemakaian = Pemakaian::find($id);
    $pemakaian->delete();

    //Menghapus data detail pemakaian yang otomatis update stok Bahan Baku
    $detail    = DPemakaian::where('id_pemakaian', '=', $id)->get();
    foreach($detail as $data)
    {
      $bahan_baku        = BahanBaku::where('kode_bahan_baku', '=', $data->kode_bahan_baku)->first();
      $bahan_baku->stok += $data->jumlah;
      $bahan_baku->update();
      $data->delete();
    }
  }
}