<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingController extends Controller
{
	public function index()
  {
    return view('setting.index'); 
  }

  public function edit($id)
  {
    $setting = Setting::find($id);
    echo json_encode($setting);
  }

  public function update(Request $request, $id)
  {
    $setting 					        = Setting::find($id);
    $setting->nama_perusahaan = $request['nama'];
    $setting->alamat         	= $request['alamat'];
    $setting->telpon         	= $request['telpon'];
      
    if ($request->hasFile('logo')) 
    {
      $file 		     = $request->file('logo');
      $nama_gambar   = "logo.".$file->getClientOriginalExtension();
      $lokasi 	     = public_path('images');

      $file->move($lokasi, $nama_gambar);
      $setting->logo = $nama_gambar;  
    }
    $setting->update();
  }
}
