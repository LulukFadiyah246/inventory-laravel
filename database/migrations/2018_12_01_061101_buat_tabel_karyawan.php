<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelKaryawan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan', function(Blueprint $table){
            $table->increments('id_karyawan');        
            $table->string('kode_karyawan', 20);       
            $table->string('nama', 30);         
            $table->text('alamat');         
            $table->string('telpon', 12);      
            $table->timestamps();       
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('karyawan');
    }
}
