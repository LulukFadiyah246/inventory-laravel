<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelDpembelian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dpembelian', function(Blueprint $table){
            $table->increments('id_dpembelian');         
            $table->integer('id_pembelian')->unsigned();         
            $table->bigInteger('kode_bahan_baku')->unsigned();           
            $table->bigInteger('harga_beli')->unsigned();           
            $table->integer('jumlah')->unsigned();             
            $table->bigInteger('sub_total')->unsigned();      
            $table->timestamps();         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dpembelian');
    }
}
