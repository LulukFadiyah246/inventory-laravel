<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelPemakaian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemakaian', function(Blueprint $table){
            $table->increments('id_pemakaian');        
            $table->integer('id_karyawan')->unsigned();            
            $table->integer('total_item')->unsigned();         
            $table->bigInteger('total_harga')->unsigned();                    
            $table->timestamps();       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pemakaian');
    }
}
