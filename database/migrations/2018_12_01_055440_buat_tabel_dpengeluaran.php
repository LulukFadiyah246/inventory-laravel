<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelDpengeluaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dpengeluaran', function(Blueprint $table){
            $table->increments('id_dpengeluaran');       
            $table->integer('id_pengeluaran')->unsigned();       
            $table->string('kode_bahan_baku', 5);         
            $table->bigInteger('harga_beli')->unsigned();         
            $table->integer('jumlah')->unsigned();                    
            $table->bigInteger('sub_total')->unsigned();     
            $table->timestamps();      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dpengeluaran');
    }
}
