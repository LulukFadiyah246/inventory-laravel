<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BuatTabelBahanBaku extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bahan_baku', function(Blueprint $table){
            $table->increments('id_bahan_baku');          
            $table->string('kode_bahan_baku', 5);           
            $table->integer('id_kategori')->unsigned();           
            $table->string('nama_bahan_baku', 20);           
            $table->enum('satuan', ['kg', 'bungkus', 'gram', 'liter', 'butir', 'ikat', 'ekor', 'biji'])->default('kg');             
            $table->bigInteger('harga_beli')->unsigned();         
            $table->integer('diskon')->unsigned();                    
            $table->integer('stok')->unsigned();      
            $table->timestamps();         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bahan_baku');
    }
}
