<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('setting')->insert(array(
	        [
	         'nama_perusahaan' => 'CV Buana Citra Sentosa', 
	         'alamat' 		   => 'Jl.Janti No. 330 Yogyakarta, 55281',
	         'telpon' 		   => '0274564734',
	         'logo' 		   => 'logo.png',
	        ]
       	));
    }
}
