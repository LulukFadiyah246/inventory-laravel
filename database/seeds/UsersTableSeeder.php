<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            [
             'name' 	=> 'Luluk Fadiyah', 
             'email' 	=> 'lulukfadiyah246@gmail.com',
             'password' => bcrypt('bismillah'),
             'telpon' 	=> '082140000664',
             'foto' 	=> 'user.png',
             'level' 	=> 1
            ],

            [
             'name' 	=> 'Fafa Mirza', 
             'email' 	=> 'fafamirza246@gmail.com',
             'password' => bcrypt('bismillah'),
             'telpon' 	=> '082242450218',
             'foto' 	=> 'user.png',
             'level' 	=> 2
            ],

            [
             'name' 	=> 'Arsen Sasongko', 
             'email' 	=> 'arsen123@gmail.com',
             'password' => bcrypt('bismillah'),
             'telpon' 	=> '085329179515',
             'foto' 	=> 'user.png',
             'level' 	=> 3
            ]
        ));
    }
}
