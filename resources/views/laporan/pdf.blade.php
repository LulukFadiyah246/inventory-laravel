<!DOCTYPE html>
<html>
<head>  
  <title>Laporan Global</title>
  <link rel="stylesheet" href="{{ asset('public/adminLTE/bootstrap/css/bootstrap.min.css') }}">
</head>
<body>
  <h3 class="text-center">Laporan Global Bahan Baku pada CV Buana Citra Sentosa</h3>
  <h4 class="text-center">Tanggal  {{ tanggal_indonesia($tanggal_awal) }} s/d {{ tanggal_indonesia($tanggal_akhir) }} </h4>     
  <table class="table table-striped">
    <thead>
      <tr>
        <th>No</th>
        <th>Tanggal</th>
        <th>Pembelian</th>
        <th>Pengeluaran</th>
        <th>Pemakaian</th>
      </tr>
    <tbody>
      @foreach($data as $row)    
        <tr>
        @foreach($row as $col)
          <td>{{ $col }}</td>
        @endforeach
        </tr>
      @endforeach
    </tbody>
  </table>
</body>
</html>