<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'BCS') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        window.BCS  = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
    <link rel="icon" type="image/png" href="{{ asset('public/images/logo.png') }}" sizes="16x16">
    <link rel="stylesheet" href="{{ asset('public/adminLTE/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/adminLTE/dist/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/adminLTE/dist/css/skins/skin-purple.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/adminLTE/plugins/datatables/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('public/adminLTE/plugins/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/sweetalert/sweetalert.css') }}">
</head>
<body class="hold-transition skin-purple sidebar-mini">
    <div class="wrapper">

    <!-- Header -->
    <header class="main-header">

        <a href="#" class="logo">
          <span class="logo-mini"><b>BCS</b></span>
         <span class="logo-lg">CV <b>BCS</b></span>
        </a>

        <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ asset('public/images/'.Auth::user()->foto) }}" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <img src="{{ asset('public/images/'.Auth::user()->foto) }}" class="img-circle" alt="User Image">
                                <p>
                                    {{ Auth::user()->name }}
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a class="btn btn-default btn-flat" href="{{ route('user.profil') }}">Edit Profil</a>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- End Header -->


    <!-- Sidebar -->
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <li class="header">MENU NAVIGASI</li>
                <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                @if( Auth::user()->level == 1 )
                    <li><a href="{{ route('kategori.index') }}"><i class="fa fa-cube"></i> <span>Kategori</span></a></li>
                    <li><a href="{{ route('supplier.index') }}"><i class="fa fa-truck"></i> <span>Supplier</span></a></li>
                    <li><a href="{{ route('bahan_baku.index') }}"><i class="fa fa-cubes"></i> <span>Bahan Baku</span></a></li>
                    <li><a href="{{ route('karyawan.index') }}"><i class="fa fa-credit-card"></i> <span>Karyawan</span></a></li>
                    <li><a href="{{ route('jenis_pengeluaran.index') }}"><i class="fa fa-credit-card"></i> <span>Jenis Pengeluaran</span></a></li>
                    <li><a href="{{ route('pembelian.index') }}"><i class="fa fa-download"></i> <span>Pembelian</span></a></li>
                    <li><a href="{{ route('pengeluaran.index') }}"><i class="fa fa-upload"></i> <span>Pengeluaran</span></a></li>
                    <li><a href="{{ route('pemakaian.index') }}"><i class="fa fa-upload"></i> <span>Pemakaian</span></a></li>
                    <li><a href="{{ route('user.index') }}""><i class="fa fa-user"></i> <span>User</span></a></li>       
                    <style type="text/css">
                        .treeview-menu 
                        {
                            display: block;
                        }
                    </style>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-file-pdf-o"></i>
                                <span>Laporan</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('laporan.index') }}">
                                    <span>Laporan Global</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('lpembelian.index') }}">
                                    <span>Laporan Pembelian</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('lpengeluaran.index') }}">
                                    <span>Laporan Pengeluaran</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('lpemakaian.index') }}">
                                    <span>Laporan Pemakaian</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="{{ route('setting.index') }}"><i class="fa fa-gears"></i> <span>Setting</span></a></li>
                @else     
                    <style type="text/css">
                        .treeview-menu 
                        {
                            display: block;
                        }
                    </style>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-file-pdf-o"></i>
                                <span>Laporan</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('laporan.index') }}">
                                    <span>Laporan Global</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('lpembelian.index') }}">
                                    <span>Laporan Pembelian</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('lpengeluaran.index') }}">
                                    <span>Laporan Pengeluaran</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('lpemakaian.index') }}">
                                    <span>Laporan Pemakaian</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </section>
    </aside>
    <!-- End Sidebar -->

    <!-- Content  -->
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                @yield('title')
            </h1>
            <ol class="breadcrumb">
                @section('breadcrumb')
                    <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                @show
            </ol>
        </section>
        <section class="content">
            @yield('content')
        </section>
    </div>
    <!-- End Content -->

    <!-- Footer -->
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
          CV Buana Citra Sentosa
        </div>
        <strong>Copyright &copy; 2018 <a href="#">Company</a>.</strong> All rights reserved.
    </footer>
    <!-- End Footer -->
     
    <script src="{{ asset('public/adminLTE/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <script src="{{ asset('public/adminLTE/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/adminLTE/dist/js/app.min.js') }}"></script>

    <script src="{{ asset('public/adminLTE/plugins/chartjs/Chart.min.js') }}"></script>
    <script src="{{ asset('public/adminLTE/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/adminLTE/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/adminLTE/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('public/js/validator.min.js') }}"></script>
    <script src="{{ asset('public/sweetalert/sweetalert.min.js') }}"></script>

    @include('sweet::alert')
    @yield('script')

</body>
</html>