<div class="modal" id="modal-form" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <form class="form-horizontal" data-toggle="validator" method="post">
         {{ csrf_field() }} {{ method_field('POST') }}
         
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
               <h3 class="modal-title"></h3>
            </div>
                     
            <div class="modal-body">            
               <input type="hidden" id="id" name="id">
               <div class="form-group">
                  <label for="kode" class="col-md-3 control-label">Kode Karyawan</label>
                  <div class="col-md-6">
                     <input id="kode" type="text" maxlength="20" class="form-control" name="kode" autofocus required>
                     <span class="help-block with-errors"></span>
                  </div>
               </div>
               <div class="form-group">
                  <label for="nama" class="col-md-3 control-label">Nama Karyawan</label>
                  <div class="col-md-6">
                     <input id="nama" type="text" maxlength="30" class="form-control" name="nama" autofocus required>
                     <span class="help-block with-errors"></span>
                  </div>
               </div>
               <div class="form-group">
                  <label for="alamat" class="col-md-3 control-label">Alamat</label>
                  <div class="col-md-8">
                     <input id="alamat" type="text" class="form-control" name="alamat" required>
                     <span class="help-block with-errors"></span>
                  </div>
               </div>
               <div class="form-group">
                  <label for="telpon" class="col-md-3 control-label">Telpon</label>
                  <div class="col-md-6">
                     <input id="telpon" type="text" maxlength="12" onkeypress="return hanyaAngka(event, false)" class="form-control" name="telpon" autofocus required>
                     <span class="help-block with-errors"></span>
                  </div>
               </div>
            </div>
            
            <div class="modal-footer">
               <button type="submit" class="btn btn-primary btn-save"><i class="fa fa-floppy-o"></i> Simpan </button>
               <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Batal</button>
            </div>
            
         </form>
      </div>
   </div>
</div>

<script language="javascript">
  function hanyaAngka(e, decimal) 
  {
    var key;
    var keychar;
    
    if(window.event) 
    {
      key   = window.event.keyCode;
    } 
    else if(e) 
    {
        key = e.which;
    } 
    else return true;
   
    keychar = String.fromCharCode(key);
    if( (key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) 
    {
      return true;
    } 
    else if ( (("0123456789").indexOf(keychar) > -1) ) 
    {
      return true;
    } 
    else if(decimal && (keychar == ".")) 
    {
      return true;
    } 
    else return false;
  }
</script>