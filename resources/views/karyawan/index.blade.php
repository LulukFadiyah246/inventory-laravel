@extends('layouts.app')

@section('title')
  Daftar Karyawan
@endsection

@section('breadcrumb')
  @parent
  <li>karyawan</li>
@endsection

@section('content')     
  <div class="row">
    <div class="col-xs-12">
      <div class="box">

        <div class="box-header">
          <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah</a>
        </div>

        <div class="box-body"> 
          <form method="post" id="form-karyawan">
          {{ csrf_field() }}
            <table class="table table-striped">
              <thead>
                <tr>
                  <th width="20"><input type="checkbox" value="1" id="select-all"></th>
                  <th width="20">No</th>
                  <th>Kode Karyawan</th>
                  <th>Nama Karyawan</th>
                  <th>Alamat</th>
                  <th>Telpon</th>
                  <th width="100">Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </form>
        </div>

      </div>
    </div>
  </div>
  @include('karyawan.form')
@endsection

@section('script')
  <script type="text/javascript">
  var table, save_method;
  $(function(){

    //Menampilkan data dengan plugin DataTable
    table = $('.table').DataTable({
      "processing"  : true,
      "ajax"        : 
      {
        "url"       : "{{ route('karyawan.data') }}",
        "type"      : "GET"
      },
      'columnDefs'  :
      [{
        'targets'   : 0,
        'searchable': false,
        'orderable' : false
      }],
      'order'       : [1, 'asc']
    }); 

    $('#select-all').click(function(){
      $('input[type="checkbox"]').prop('checked', this.checked);
    });
     
    //Menyimpan data form tambah/edit beserta validasinya
    $('#modal-form form').on('submit', function(e){
      if(!e.isDefaultPrevented())
      {
        var id                       = $('#id').val();
        if(save_method == "add") url = "{{ route('karyawan.store') }}";
        else url                     = "karyawan/"+id;
           
          $.ajax({
            url      : url,
            type     : "POST",
            data     : $('#modal-form form').serialize(),
            dataType : 'JSON',
            success  : function(data)
            {
              if(data.msg=="error")
              {
                swal("Kode karyawan sudah digunakan!", "", "warning");
                $('#kode').focus().select();
              }
              else
              {
                swal("Karyawan Berhasil Disimpan!", "", "success");
                $('#modal-form').modal('hide');
                table.ajax.reload();
              }
            },
            error   : function()
            {
              swal("Tidak dapat menyimpan data!", "", "warning");
            }   
          });
        return false;
      }
    });
  });

  //Menampilkan form tambah
  function addForm()
  {
    save_method = "add";
    $('input[name=_method]').val('POST');
    $('#modal-form').modal('show');
    $('#modal-form form')[0].reset();            
    $('.modal-title').text('Tambah Karyawan');
    $('#kode').attr('readonly', false);
  }

  //Menampilkan form edit dan menampilkan data pada form tersebut
  function editForm(id)
  {
    save_method = "edit";
    $('input[name=_method]').val('PATCH');
    $('#modal-form form')[0].reset();
    $.ajax({
      url      : "karyawan/"+id+"/edit",
      type     : "GET",
      dataType : "JSON",
      success  : function(data)
      {
        $('#modal-form').modal('show');
        $('.modal-title').text('Edit Karyawan');
         
        $('#id').val(data.id_karyawan);
        $('#kode').val(data.kode_karyawan).attr('readonly', true);
        $('#nama').val(data.nama);
        $('#alamat').val(data.alamat);
        $('#telpon').val(data.telpon);
      },
      error   : function()
      {
        swal("Tidak dapat menampilkan data!", "", "warning");
      }
    });
  }

  //Menghapus Data
  function deleteData(id)
  {
    swal({
      title               : "Anda Yakin?",
      text                : "Hapus Karyawan?",
      icon                : "warning",
      showCancelButton    : true,
      confirmButtonColor  : "DD6B55",
      confirmButtonText   : "YA, Hapus",
      closeOnConfirm      : false
    }, 
    function(isConfirm)
    {
      if (!isConfirm) return;
      $.ajax({
        url   : "karyawan/"+id,
        type  : "POST",
        data  : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
        success: function (data) 
        {
          swal("Berhasil!", "Karyawan berhasil dihapus!", "success");
          table.ajax.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) 
        {
          swal("Gagal Menghapus!", "Silahkan coba lagi!", "error");
        }
      });
    });
  }
  </script>
@endsection