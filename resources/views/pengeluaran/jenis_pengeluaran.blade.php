<div class="modal" id="modal-jenis_pengeluaran" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
     
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
            <h3 class="modal-title">Pilih Jenis Pengeluaran</h3>
         </div>
               
         <div class="modal-body">
            <table class="table table-striped tabel-jenis_pengeluaran">
               <thead>
                  <tr>
                     <th>Kode Jenis Pengeluaran</th>
                     <th>Jenis Pengeluaran</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach($jenis_pengeluaran as $data)
                     <tr>
                        <th>{{ $data->kode_jenis_pengeluaran }}</th>
                        <th>{{ $data->nama }}</th>
                        <th><a href="pengeluaran/{{ $data->id_jenis_pengeluaran }}/tambah" class="btn btn-primary"><i class="fa fa-check-circle"></i> Pilih</a></th>
                     </tr>
                  @endforeach
               </tbody>
            </table>
         </div>
      
      </div>
   </div>
</div>