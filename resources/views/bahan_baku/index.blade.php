@extends('layouts.app')

@section('title')
  Daftar Bahan Baku
@endsection

@section('breadcrumb')
  @parent
  <li>bahan_baku</li>
@endsection

@section('content')     
  <div class="row">
    <div class="col-xs-12">
      <div class="box">

        <div class="box-header">
          <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah</a>
          <a onclick="deleteAll()" class="btn btn-danger"><i class="fa fa-trash"></i> Hapus Multiple</a>
          <a href="bahan_baku/lbahanbaku" target="_blank" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Cetak Laporan Bahan Baku</a>
        </div>

        <div class="box-body">  
          <form method="post" id="form-bahan_baku">
          {{ csrf_field() }}
            <table class="table table-striped">
              <thead>
                <tr>
                  <th width="20"><input type="checkbox" value="1" id="select-all"></th>
                  <th width="20">No</th>
                  <th>Kode Bahan Baku</th>
                  <th>Nama Bahan Baku</th>
                  <th>Kategori</th>
                  <th>Satuan</th>
                  <th>Harga Beli</th>
                  <th>Diskon</th>
                  <th>Stok</th>
                  <th width="100">Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </form>
        </div>

      </div>
    </div>
  </div>
  @include('bahan_baku.form')
@endsection

@section('script')
  <script type="text/javascript">
  var table, save_method;
  $(function(){

    //Menampilkan data dengan plugin DataTable
    table = $('.table').DataTable({
      "processing"  : true,
      "serverside"  : true,
      "ajax"        : 
      {
        "url"       : "{{ route('bahan_baku.data') }}",
        "type"      : "GET"
      },
      'columnDefs'  : 
      [{
        'targets'   : 0,
        'searchable': false,
        'orderable' : false
      }],
      'order'       : [1, 'asc']
    }); 
     
    //Centang semua checkbox ketika checkbox dengan id #selectall dicentang
    $('#select-all').click(function(){
      $('input[type="checkbox"]').prop('checked', this.checked);
    });

    //Menyimpan data dari form tambah/edit beserta validasinya
    $('#modal-form form').on('submit', function(e){
      if(!e.isDefaultPrevented())
      {
        var id                       = $('#id').val();
        if(save_method == "add") url = "{{ route('bahan_baku.store') }}";
        else url                     = "bahan_baku/"+id;
           
          $.ajax({
            url       : url,
            type      : "POST",
            data      : $('#modal-form form').serialize(),
            dataType  : 'JSON',
            success   : function(data)
            {
              if(data.msg=="error")
              {
                swal("Kode Bahan Baku sudah digunakan!", "", "warning");
                $('#kode').focus().select();
              }
              else
              {
                swal("Bahan Baku Berhasil Disimpan!", "", "success");
                $('#modal-form').modal('hide');
                table.ajax.reload();
              }            
            },
            error     : function()
            {
              swal("Tidak dapat menyimpan data!", "", "warning");
            }   
          });
        return false;
      }
    });
  });

  //Menampilkan form tambah data
  function addForm()
  {
    save_method = "add";
    $('input[name=_method]').val('POST');
    $('#modal-form').modal('show');
    $('#modal-form form')[0].reset();            
    $('.modal-title').text('Tambah Bahan Baku');
    $('#kode').attr('readonly', false);
  }

  //Menampilkan form edit dan menampilkan data pada form tersebut
  function editForm(id)
  {
    save_method = "edit";
    $('input[name=_method]').val('PATCH');
    $('#modal-form form')[0].reset();
    $.ajax({
      url      : "bahan_baku/"+id+"/edit",
      type     : "GET",
      dataType : "JSON",
      success  : function(data)
      {
        $('#modal-form').modal('show');
        $('.modal-title').text('Edit Bahan Baku');
         
        $('#id').val(data.id_bahan_baku);
        $('#kode').val(data.kode_bahan_baku).attr('readonly', true);
        $('#nama').val(data.nama_bahan_baku);
        $('#kategori').val(data.id_kategori);
        $('#satuan').val(data.satuan);
        $('#harga_beli').val(data.harga_beli);
        $('#diskon').val(data.diskon);
        $('#stok').val(data.stok);
      },
      error    : function()
      {
        swal("Tidak dapat menampilkan data!", "", "warning");
      }
    });
  }

  //Menghapus data
  function deleteData(id)
  {
    swal({
      title               : "Anda Yakin?",
      text                : "Hapus Bahan Baku?",
      icon                : "warning",
      showCancelButton    : true,
      confirmButtonColor  : "DD6B55",
      confirmButtonText   : "YA, Hapus",
      closeOnConfirm      : false
    }, 
    function(isConfirm)
    {
      if (!isConfirm) return;
      $.ajax({
        url   : "bahan_baku/"+id,
        type  : "POST",
        data  : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
        success: function (data) 
        {
          swal("Berhasil!", "Bahan Baku berhasil dihapus!", "success");
          table.ajax.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) 
        {
          swal("Gagal Menghapus!", "Silahkan coba lagi!", "error");
        }
      });
    });
  }

  //Menghapus semua data yang dicentang
  function deleteAll(id)
  {
    swal({
      title               : "Anda Yakin?",
      text                : "Hapus Bahan Baku terpilih?",
      icon                : "warning",
      showCancelButton    : true,
      confirmButtonColor  : "DD6B55",
      confirmButtonText   : "YA, Hapus",
      closeOnConfirm      : false
    }, 
    function(isConfirm)
    {
      if($('input:checked').length < 1)
      {
        swal("Pilih data yang akan dihapus!", '', 'error');
      }
      else if (!isConfirm) return;
      $.ajax({
        url   : "bahan_baku/hapus",
        type  : "POST",
        data  : $('#form-bahan_baku').serialize(),
        success: function (data) 
        {
          swal("Berhasil!", "Bahan Baku berhasil dihapus!", "success");
          table.ajax.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) 
        {
          swal("Gagal Menghapus!", "Silahkan coba lagi!", "error");
        }
      });
    });
  }
  </script>
@endsection 