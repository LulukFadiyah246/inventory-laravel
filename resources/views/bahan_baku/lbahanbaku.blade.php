<!DOCTYPE html>
<html>
<head>  
  <title>Laporan Bahan Baku</title>
  <link rel="stylesheet" href="{{ asset('public/adminLTE/bootstrap/css/bootstrap.min.css') }}">
</head>
<body>
  <h2 class="text-center">Laporan Bahan Baku</h3>
  <h3 class="text-center">CV Buana Citra Sentosa</h4>         
  <table class="table table-striped">
  <thead>
    <tr>
      <th>Kode Bahan Baku</th>
      <th>ID Kategori</th>
      <th>Nama Bahan Baku</th>
      <th>Satuan</th>
      <th>Harga Beli</th>
      <th>Diskon</th>
      <th>Stok</th>
    </tr>
    <tbody>
      @foreach($bahan_baku as $bb)    
        <tr>
          <td>{{ $bb->kode_bahan_baku }}</td>
          <td>{{ $bb->id_kategori }}</td>
          <td>{{ $bb->nama_bahan_baku }}</td>
          <td>{{ $bb->satuan }}</td>
          <td>Rp. {{ format_uang($bb->harga_beli) }}</td>
          <td>{{ $bb->diskon }}%</td>
          <td>{{ $bb->stok }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>
</body>
</html>