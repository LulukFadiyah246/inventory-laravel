<!DOCTYPE html>
<html>
<head>  
  <title>Laporan Pembelian</title>
  <link rel="stylesheet" href="{{ asset('public/adminLTE/bootstrap/css/bootstrap.min.css') }}">
</head>
<body>
  <h3 class="text-center">Laporan Pembelian Bahan Baku pada CV Buana Citra Sentosa</h3>         
  <table class="table table-striped">
    <thead>
      <tr>
        <th>No</th>
        <th>Tanggal</th>
        <th>Supplier</th>
        <th>Total Item</th>
        <th>Total Harga</th>
        <th>Diskon</th>
        <th>Total Bayar</th>
      </tr>
    <tbody>
      @foreach($data as $row)    
        <tr>
        @foreach($row as $col)
          <td>{{ $col }}</td>
        @endforeach
        </tr>
      @endforeach
    </tbody>
  </table>
</body>
</html>
