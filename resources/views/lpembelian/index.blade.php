@extends('layouts.app')

@section('title')
  Laporan Pembelian
@endsection

@section('breadcrumb')
  @parent
  <li>laporan_pembelian</li>
@endsection

@section('content')     
  <div class="row">
    <div class="col-xs-12">
      <div class="box">

        <div class="box-header">
          <a href="lpembelian/pdf/{{$awal}}/{{$akhir}}" target="_blank" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export PDF</a>
        </div>

        <div class="box-body">  
          <table class="table table-striped tabel-lpembelian">
            <thead>
              <tr>
                <th width="30">No</th>
                <th>Tanggal</th>
                <th>Supplier</th>
                <th>Total Item</th>
                <th>Total Harga</th>
                <th>Diskon</th>
                <th>Total Bayar</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>

      </div>
    </div>
  </div>
  @include('lpembelian.form')
@endsection

@section('script')
  <script type="text/javascript">
  var table, awal, akhir;
  $(function(){
    $('#awal, #akhir').datepicker({
      format        : 'yyyy-mm-dd',
      autoclose     : true
    });

    table = $('.tabel-lpembelian').DataTable({
      "dom"         : 'Brt',
      "bSort"       : false,
      "bPaginate"   : false,
      "processing"  : true,
      "serverside"  : true,
      "ajax"        : 
      {
        "url"       : "lpembelian/data/{{ $awal }}/{{ $akhir }}",
        "type"      : "GET"
      }
    }); 
  });

  function periodeForm()
  {
    $('#modal-form').modal('show');        
  }
  </script>
@endsection