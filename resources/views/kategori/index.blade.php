@extends('layouts.app')

@section('title')
  Daftar Kategori
@endsection

@section('breadcrumb')
  @parent
  <li>kategori</li>
@endsection

@section('content')     
  <div class="row">
    <div class="col-xs-12">
      <div class="box">

        <div class="box-header">
          <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah</a>
        </div>

        <div class="box-body">  
          <table class="table table-striped">
            <thead>
              <tr>
                <th width="30">No</th>
                <th>Nama Kategori</th>
                <th width="100">Aksi</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>
        
      </div>
    </div>
  </div>
  @include('kategori.form')
@endsection

@section('script')
  <script type="text/javascript">
  var table, save_method;
  $(function(){

    //Menampilkan data dengan plugin DataTable
    table = $('.table').DataTable({
      "processing" : true,
      "ajax"       : 
      {
        "url"      : "{{ route('kategori.data') }}",
        "type"     : "GET"
      }
    }); 
    
    //Menyimpan data form tambah/edit beserta validasinya
    $('#modal-form form').on('submit', function(e){
      if(!e.isDefaultPrevented())
      {
        var id                       = $('#id').val();
        if(save_method == "add") url = "{{ route('kategori.store') }}";
        else url                     = "kategori/"+id;

          $.ajax({
            url     : url,
            type    : "POST",
            data    : $('#modal-form form').serialize(),
            dataType: 'JSON',
            success : function(data)
            {
              if(data.msg=="error")
              {
                swal("Kategori tidak boleh sama!", "", "warning");
                $('#nama').focus().select();
              }
              else
              {
                swal("Kategori Berhasil Disimpan!", "", "success");
                $('#modal-form').modal('hide');
                table.ajax.reload();
              }
            },
            error   : function()
            {
              swal("Tidak dapat menyimpan data!", "", "warning");
            }   
          });
        return false;
      }
    });
  });

  //Menampilkan form tambah
  function addForm()
  {
    save_method = "add";
    $('input[name=_method]').val('POST');
    $('#modal-form').modal('show');
    $('#modal-form form')[0].reset();            
    $('.modal-title').text('Tambah Kategori');
  }

  //Menampilkan form edit dan menampilkan data pada form tersebut
  function editForm(id)
  {
    save_method = "edit";
    $('input[name=_method]').val('PATCH');
    $('#modal-form form')[0].reset();
    $.ajax({
      url      : "kategori/"+id+"/edit",
      type     : "GET",
      dataType : "JSON",
      success  : function(data)
      {
        $('#modal-form').modal('show');
        $('.modal-title').text('Edit Kategori');
         
        $('#id').val(data.id_kategori);
        $('#nama').val(data.nama_kategori);
      },
      error    : function()
      {
        swal("Tidak dapat menampilkan data!", "", "warning");
      }
    });
  }

  //Menghapus Data
  function deleteData(id)
  {
    swal({
      title               : "Anda Yakin?",
      text                : "Hapus Kategori?",
      icon                : "warning",
      showCancelButton    : true,
      confirmButtonColor  : "DD6B55",
      confirmButtonText   : "YA, Hapus",
      closeOnConfirm      : false
    }, 
    function(isConfirm)
    {
      if (!isConfirm) return;
      $.ajax({
        url   : "kategori/"+id,
        type  : "POST",
        data  : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
        success: function (data) 
        {
          swal("Berhasil!", "Kategori berhasil dihapus!", "success");
          table.ajax.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) 
        {
          swal("Gagal Menghapus!", "Silahkan coba lagi!", "error");
        }
      });
    });
  }
  </script>
@endsection