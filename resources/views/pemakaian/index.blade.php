@extends('layouts.app')

@section('title')
  Daftar Pemakaian
@endsection

@section('breadcrumb')
  @parent
  <li>pemakaian</li>
@endsection 

@section('content')     
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        
        <div class="box-header">
          <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Transaksi Baru</a>
          @if(!empty(session('idpemakaian')))
          <a href="{{ route('dpemakaian.index') }}" class="btn btn-info"><i class="fa fa-plus-circle"></i> Transaksi Aktif</a>
          @endif
        </div>

        <div class="box-body">  
          <table class="table table-striped tabel-pemakaian">
            <thead>
              <tr>
                <th width="30">No</th>
                <th>Tanggal</th>
                <th>Nama Karyawan</th>
                <th>Total Item</th>
                <th>Total Harga</th>
                <th width="100">Aksi</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>

      </div>
    </div>
  </div>
  @include('pemakaian.detail')
  @include('pemakaian.karyawan')
@endsection

@section('script')
  <script type="text/javascript">
  var table, save_method, table1;
  $(function(){

    //Menampilkan data dengan plugin DataTable
    table  = $('.tabel-pemakaian').DataTable({
      "processing" : true,
      "serverside" : true,
      "ajax"       : 
      {
        "url"      : "{{ route('pemakaian.data') }}",
        "type"     : "GET"
      }
    }); 
     
    //Menampilkan data dengan plugin DataTable
    table1 = $('.tabel-detail').DataTable({
      "dom"        : 'Brt',
      "bSort"      : false,
      "processing" : true
    });

    //Menampilkan data dengan plugin DataTable
    $('.tabel-karyawan').DataTable();

    //Menyimpan data dari form tambah/edit
    $('#modal-form form').validator().on('submit', function(e){
      if(!e.isDefaultPrevented())
      {
        var id                       = $('#id').val();
        if(save_method == "add") url = "{{ route('dpemakaian.store') }}";
        else url                     = "dpemakaian/"+id;
          $.ajax({
            url       : url,
            type      : "POST",
            data      : $('#modal-form form').serialize(),
            dataType  : 'JSON',
            success   : function(data)
            {
              $('#stok').focus().select(); 
            },
            error     : function()
            {
              swal("Tidak dapat menyimpan data!", "", "warning");
            }   
          });
        return false;
      }
    });

  });

  function addForm()
  {
    $('#modal-karyawan').modal('show');        
  }

  function showDetail(id)
  {
    $('#modal-detail').modal('show');

    table1.ajax.url("pemakaian/"+id+"/lihat");
    table1.ajax.reload();
  }

  //Menghapus Data
  function deleteData(id)
  {
    swal({
      title               : "Anda Yakin?",
      text                : "Hapus Pemakaian?",
      icon                : "warning",
      showCancelButton    : true,
      confirmButtonColor  : "DD6B55",
      confirmButtonText   : "YA, Hapus",
      closeOnConfirm      : false
    }, 
    function(isConfirm)
    {
      if (!isConfirm) return;
      $.ajax({
        url   : "pemakaian/"+id,
        type  : "POST",
        data  : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
        success: function (data) 
        {
          swal("Berhasil!", "Pemakaian berhasil dihapus!", "success");
          table.ajax.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) 
        {
          swal("Gagal Menghapus!", "Silahkan coba lagi!", "error");
        }
      });
    });
  }

  </script>
@endsection