@extends('layouts.app')

@section('title')
  Dashboard
@endsection

@section('breadcrumb')
  @parent  
  <li>dashboard</li>
@endsection

@section('content') 
<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-body text-center">
        <h1>Selamat Datang</h1>
        <br><br>
        <h2>Anda login sebagai Umum</h2>
        <br><br>
            <a class="btn btn-success btn-lg" href="{{ route('laporan.index') }}">Laporan Global</a>
            <a class="btn btn-success btn-lg" href="{{ route('lpembelian.index') }}">Laporan Pembelian</a>
            <a class="btn btn-success btn-lg" href="{{ route('lpengeluaran.index') }}">Laporan Pengeluaran</a>
            <a class="btn btn-success btn-lg" href="{{ route('lpemakaian.index') }}">Laporan Pemakaian</a>
            <br><br><br>
      </div>
    </div>
  </div>
</div>
@endsection