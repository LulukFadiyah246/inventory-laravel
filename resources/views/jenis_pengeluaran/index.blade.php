@extends('layouts.app')

@section('title')
  Daftar Jenis Pengeluaran
@endsection

@section('breadcrumb')
  @parent
  <li>jenis_pengeluaran</li>
@endsection

@section('content')     
  <div class="row">
    <div class="col-xs-12">
      <div class="box">

        <div class="box-header">
          <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Tambah</a>
        </div>

        <div class="box-body"> 
          <form method="post" id="form-jenis_pengeluaran">
          {{ csrf_field() }}
            <table class="table table-striped">
              <thead>
                <tr>
                  <th width="20">No</th>
                  <th>Kode Jenis Pengeluaran</th>
                  <th>Nama Jenis Pengeluaran</th>
                  <th width="100">Aksi</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </form>
        </div>

      </div>
    </div>
  </div>
  @include('jenis_pengeluaran.form')
@endsection

@section('script')
  <script type="text/javascript">
  var table, save_method;
  $(function(){

    //Menampilkan data dengan plugin DataTable
    table = $('.table').DataTable({
      "processing"  : true,
      "ajax"        : 
      {
        "url"       : "{{ route('jenis_pengeluaran.data') }}",
        "type"      : "GET"
      },
      'columnDefs'  :
      [{
        'targets'   : 0,
        'searchable': false,
        'orderable' : false
      }],
      'order'       : [1, 'asc']
    }); 
     
    //Menyimpan data form tambah/edit beserta validasinya
    $('#modal-form form').on('submit', function(e){
      if(!e.isDefaultPrevented())
      {
        var id                       = $('#id').val();
        if(save_method == "add") url = "{{ route('jenis_pengeluaran.store') }}";
        else url                     = "jenis_pengeluaran/"+id;
           
          $.ajax({
            url      : url,
            type     : "POST",
            data     : $('#modal-form form').serialize(),
            dataType : 'JSON',
            success  : function(data)
            {
              if(data.msg=="error")
              {
                swal("Kode Jenis Pengeluaran sudah digunakan!", "", "warning");
                $('#kode').focus().select();
              }
              else
              {
                swal("Jenis Pengeluaran Berhasil Disimpan!", "", "success");
                $('#modal-form').modal('hide');
                table.ajax.reload();
              }
            },
            error   : function()
            {
              swal("Tidak dapat menyimpan data!", "", "warning");
            }   
          });
        return false;
      }
    });
  });

  //Menampilkan form tambah
  function addForm()
  {
    save_method = "add";
    $('input[name=_method]').val('POST');
    $('#modal-form').modal('show');
    $('#modal-form form')[0].reset();            
    $('.modal-title').text('Tambah Jenis Pengeluaran');
    $('#kode').attr('readonly', false);
  }

  //Menampilkan form edit dan menampilkan data pada form tersebut
  function editForm(id)
  {
    save_method = "edit";
    $('input[name=_method]').val('PATCH');
    $('#modal-form form')[0].reset();
    $.ajax({
      url      : "jenis_pengeluaran/"+id+"/edit",
      type     : "GET",
      dataType : "JSON",
      success  : function(data)
      {
        $('#modal-form').modal('show');
        $('.modal-title').text('Edit Jenis Pengeluaran');
         
        $('#id').val(data.id_jenis_pengeluaran);
        $('#kode').val(data.kode_jenis_pengeluaran).attr('readonly', true);
        $('#nama').val(data.nama);
      },
      error   : function()
      {
        swal("Tidak dapat menampilkan data!", "", "warning");
      }
    });
  }

  //Menghapus Data
  function deleteData(id)
  {
    swal({
      title               : "Anda Yakin?",
      text                : "Hapus Jenis Pengeluaran?",
      icon                : "warning",
      showCancelButton    : true,
      confirmButtonColor  : "DD6B55",
      confirmButtonText   : "YA, Hapus",
      closeOnConfirm      : false
    }, 
    function(isConfirm)
    {
      if (!isConfirm) return;
      $.ajax({
        url   : "jenis_pengeluaran/"+id,
        type  : "POST",
        data  : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
        success: function (data) 
        {
          swal("Berhasil!", "Jenis Pengeluaran berhasil dihapus!", "success");
          table.ajax.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) 
        {
          swal("Gagal Menghapus!", "Silahkan coba lagi!", "error");
        }
      });
    });
  }
  </script>
@endsection 