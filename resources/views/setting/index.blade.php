@extends('layouts.app')

@section('title')
  Pengaturan
@endsection

@section('breadcrumb')
  @parent
  <li>pengaturan</li>
@endsection

@section('content')     
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <form class="form form-horizontal" data-toggle="validator" method="post" enctype="multipart/form-data">
        {{ csrf_field() }} {{ method_field('PATCH') }}
          
          <div class="box-body">
            <div class="alert alert-info alert-dismissible" style="display:none">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <i class="icon fa fa-check"></i>
              Perubahan berhasil disimpan.
            </div>
            <div class="form-group">
              <label for="nama" class="col-md-2 control-label">Nama Perusahaan</label>
              <div class="col-md-6">
                <input id="nama" type="text" maxlength="30" class="form-control" name="nama" required>
                <span class="help-block with-errors"></span>
              </div>
            </div>
            <div class="form-group">
              <label for="alamat" class="col-md-2 control-label">Alamat</label>
              <div class="col-md-10">
                <input id="alamat" type="text" class="form-control" name="alamat" required>
                <span class="help-block with-errors"></span>
              </div>
            </div>
            <div class="form-group">
              <label for="telpon" class="col-md-2 control-label">Telpon</label>
              <div class="col-md-4">
                <input id="telpon" type="text" maxlength="12" onkeypress="return hanyaAngka(event, false)" class="form-control" name="telpon" required>
                <span class="help-block with-errors"></span>
              </div>
            </div>
            <div class="form-group">
              <label for="logo" class="col-md-2 control-label">Logo Perusahaan</label>
              <div class="col-md-4">
                <input id="logo" type="file" class="form-control" name="logo">
                <br><div class="tampil-logo"></div>
              </div>
            </div>
          </div>

          <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Simpan Perubahan</button>
          </div>

        </form>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
    $(function(){
      showData();
      $('.form').on('submit', function(e){
        if(!e.isDefaultPrevented())
        { 
          $.ajax({
            url         : "setting/1",
            type        : "POST",
            data        : new FormData($(".form")[0]),
            async       : false,
            processData : false,
            contentType : false,
            success     : function(data)
            {
              showData();
              $('.alert').css('display', 'block').delay(2000).fadeOut();
            },
            error       : function()
            {
              alert("Tidak dapat menyimpan data!");
            }   
          });
          return false;
        }
      });
    });

    function showData()
    {
      $.ajax({
        url       : "setting/1/edit",
        type      : "GET",
        dataType  : "JSON",
        success   : function(data)
        {
          $('#nama').val(data.nama_perusahaan);
          $('#alamat').val(data.alamat);
          $('#telpon').val(data.telpon);

          d = new Date();
          $('.tampil-logo').html('<img src="public/images/'+data.logo+'?'+d.getTime()+'" width="200">');
        },
        error     : function()
        {
          alert("Tidak dapat menyimpan data!");
        }   
      });
    }

    function hanyaAngka(e, decimal) 
    {
      var key;
      var keychar;
      
      if(window.event) 
      {
        key   = window.event.keyCode;
      } 
      else if(e) 
      {
          key = e.which;
      } 
      else return true;
     
      keychar = String.fromCharCode(key);
      if( (key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) 
      {
        return true;
      } 
      else if ( (("0123456789").indexOf(keychar) > -1) ) 
      {
        return true;
      } 
      else if(decimal && (keychar == ".")) 
      {
        return true;
      } 
      else return false;
    }
  </script>
@endsection