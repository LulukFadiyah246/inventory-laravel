@extends('layouts.app')

@section('title')
  Edit Profil
@endsection

@section('breadcrumb')
  @parent
  <li>user</li>
  <li>edit_profil</li>
@endsection

@section('content')     
  <div class="row">
    <div class="col-xs-12">
      <div class="box">

        <form class="form form-horizontal" data-toggle="validator" method="post" enctype="multipart/form-data">
        {{ csrf_field() }} {{ method_field('PATCH') }}
       
          <div class="box-body">
            <div class="alert alert-info alert-dismissible" style="display:none">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <i class="icon fa fa-check"></i>
              Perubahan berhasil disimpan.
            </div>
            <div class="form-group">
              <label for="foto" class="col-md-2 control-label">Foto Profil</label>
              <div class="col-md-4">
                <input id="foto" type="file" class="form-control" name="foto">
                <br><div class="tampil-foto"> <img src="{{ asset('public/images/'.Auth::user()->foto) }}" width="200"></div>
              </div>
            </div>
            <div class="form-group">
              <label for="emailbaru" class="col-md-2 control-label">Email</label>
              <div class="col-md-6">
                <input id="emailbaru" type="email" maxlength="50" class="form-control" name="emailbaru">
                <span class="help-block with-errors"></span>
              </div>
            </div>
            <div class="form-group">
              <label for="passwordlama" class="col-md-2 control-label">Password Lama</label>
              <div class="col-md-6">
                <input id="passwordlama" type="password" maxlength="10" class="form-control" name="passwordlama">
                <span class="help-block with-errors"></span>
              </div>
            </div>
            <div class="form-group">
              <label for="password" class="col-md-2 control-label">Password Baru</label>
              <div class="col-md-6">
                <input id="password" type="password" maxlength="10" class="form-control" name="password">
                <span class="help-block with-errors"></span>
              </div>
            </div>
            <div class="form-group">
              <label for="password1" class="col-md-2 control-label">Ulang Password</label>
              <div class="col-md-6">
                <input id="password1" type="password" maxlength="10" class="form-control" data-match="#password" name="password1">
                <span class="help-block with-errors"></span>
              </div>
            </div>
            <div class="form-group">
              <label for="telponbaru" class="col-md-2 control-label">Telpon</label>
              <div class="col-md-6">
                <input id="telponbaru" type="text" maxlength="12" onkeypress="return hanyaAngka(event, false)" class="form-control" name="telponbaru">
                <span class="help-block with-errors"></span>
              </div>
            </div>
          </div>

          <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Simpan Perubahan</button>
          </div>

        </form>

      </div>
    </div>
  </div>
@endsection

@section('script')
  <script type="text/javascript">
  $(function(){
    //Saat password lama diubah
    $('#passwordlama').keyup(function(){
      if($(this).val() != "") $('#password, #password1').attr('required', true);
      else $('#password, #password1').attr('required', false);
    });

    $('.form').on('submit', function(e){
      if(!e.isDefaultPrevented())
      { 
        //Upload file via ajax
        $.ajax({
          url         : "{{ Auth::user()->id }}/change",
          type        : "POST",
          data        : new FormData($(".form")[0]),
          dataType    : 'JSON',
          async       : false,
          processData : false,
          contentType : false,
          success     : function(data)
          {
            //Tampilkan pesan jika data msg=error
            if(data.msg == "error")
            {
              swal("Password lama salah!", "", "warning");
              $('#passwordlama').focus().select();
            }
            else
            {
              d = new Date();
              $('.alert').css('display', 'block').delay(2000).fadeOut();
              //Update foto user
              $('.tampil-foto img, .user-image, .user-header img').attr('src', data.url+'?'+d.getTime());
            }
          },
          error       : function()
          {
            swal("Tidak dapat menyimpan data!", "", "warning");
          }   
        });
        return false;
      }
    });

  });

  function hanyaAngka(e, decimal) 
  {
      var key;
      var keychar;
    
      if(window.event) 
      {
         key   = window.event.keyCode;
      } 
      else if(e) 
      {
         key = e.which;
      } 
      else return true;
   
      keychar = String.fromCharCode(key);
      if( (key==null) || (key==0) || (key==8) ||  (key==9) || (key==13) || (key==27) ) 
      {
         return true;
      } 
      else if ( (("0123456789").indexOf(keychar) > -1) ) 
      {
         return true;
      }  
      else if(decimal && (keychar == ".")) 
      {
         return true;
      } 
      else return false;
   }
  </script>
@endsection