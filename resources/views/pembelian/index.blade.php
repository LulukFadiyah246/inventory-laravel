@extends('layouts.app')

@section('title')
  Daftar Pembelian
@endsection

@section('breadcrumb')
  @parent
  <li>pembelian</li>
@endsection

@section('content')     
  <div class="row">
    <div class="col-xs-12"> 
      <div class="box">

        <div class="box-header">
          <a onclick="addForm()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Transaksi Baru</a>
          @if(!empty(session('idpembelian')))
          <a href="{{ route('dpembelian.index') }}" class="btn btn-info"><i class="fa fa-plus-circle"></i> Transaksi Aktif</a>
          @endif
        </div>

        <div class="box-body">  
          <table class="table table-striped tabel-pembelian">
            <thead>
              <tr>
                <th width="30">No</th>
                <th>Tanggal</th>
                <th>Supplier</th>
                <th>Total Item</th>
                <th>Total Harga</th>
                <th>Diskon</th>
                <th>Total Bayar</th>
                <th width="100">Aksi</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>

      </div>
    </div>
  </div>
  @include('pembelian.detail')
  @include('pembelian.supplier')
@endsection

@section('script')
  <script type="text/javascript">
  var table, save_method, table1;
  $(function(){

    //Menampilkan data dengan plugin DataTable
    table = $('.tabel-pembelian').DataTable({
      "processing" : true,
      "serverside" : true,
      "ajax"       : 
      {
        "url"      : "{{ route('pembelian.data') }}",
        "type"     : "GET"
      }
    }); 
     
    //Menampilkan data dengan plugin DataTable
    table1 = $('.tabel-detail').DataTable({
      "dom"        : 'Brt',
      "bSort"      : false,
      "processing" : true
    });

    //Menampilkan data dengan plugin DataTable
    $('.tabel-supplier').DataTable();
  });

  function addForm()
  {
    $('#modal-supplier').modal('show');        
  }

  function showDetail(id)
  {
    $('#modal-detail').modal('show');

    table1.ajax.url("pembelian/"+id+"/lihat");
    table1.ajax.reload();
  }

  //Menghapus Data
  function deleteData(id)
  {
    swal({
      title               : "Anda Yakin?",
      text                : "Hapus Pembelian?",
      icon                : "warning",
      showCancelButton    : true,
      confirmButtonColor  : "DD6B55",
      confirmButtonText   : "YA, Hapus",
      closeOnConfirm      : false
    }, 
    function(isConfirm)
    {
      if (!isConfirm) return;
      $.ajax({
        url   : "pembelian/"+id,
        type  : "POST",
        data  : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
        success: function (data) 
        {
          swal("Berhasil!", "Pembelian berhasil dihapus!", "success");
          table.ajax.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) 
        {
          swal("Gagal Menghapus!", "Silahkan coba lagi!", "error");
        }
      });
    });
  }
  </script>
@endsection