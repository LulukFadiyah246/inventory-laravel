@extends('layouts.app')

@section('title')
  Laporan Pengeluaran
@endsection

@section('breadcrumb')
  @parent
  <li>laporan_pengeluaran</li>
@endsection

@section('content')     
  <div class="row">
    <div class="col-xs-12">
      <div class="box">

        <div class="box-header">
          <a href="lpengeluaran/pdf/{{$awal}}/{{$akhir}}" target="_blank" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export PDF</a>
        </div>

        <div class="box-body">  
          <table class="table table-striped tabel-lpengeluaran">
            <thead>
              <tr>
                <th width="30">No</th>
                <th>Tanggal</th>
                <th>Jenis Pengeluaran</th>
                <th>Total Item</th>
                <th>Total Harga</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>

      </div>
    </div>
  </div>
  @include('lpengeluaran.form')
@endsection

@section('script')
  <script type="text/javascript">
  var table, awal, akhir;
  $(function(){
    $('#awal, #akhir').datepicker({
      format        : 'yyyy-mm-dd',
      autoclose     : true
    });

    table = $('.tabel-lpengeluaran').DataTable({
      "dom"         : 'Brt',
      "bSort"       : false,
      "bPaginate"   : false,
      "processing"  : true,
      "serverside"  : true,
      "ajax"        : 
      {
        "url"       : "lpengeluaran/data/{{ $awal }}/{{ $akhir }}",
        "type"      : "GET"
      }
    }); 

  });

  function periodeForm()
  {
    $('#modal-form').modal('show');        
  }
  </script>
@endsection