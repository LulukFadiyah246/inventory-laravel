<div class="modal" id="modal-bahan_baku" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
     
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"> &times; </span> </button>
            <h3 class="modal-title">Cari Bahan Baku</h3>
         </div>
            
         <div class="modal-body">
            <table class="table table-striped tabel-bahan_baku">
               <thead>
                  <tr>
                     <th>Kode Bahan Baku</th>
                     <th>Nama Bahan Baku</th>
                     <th>Harga Beli</th>
                     <th>Stok</th>
                     <th>Aksi</th>
                  </tr>
               </thead>
               <tbody>
                  @foreach($bahan_baku as $data)
                     <tr>
                        <th>{{ $data->kode_bahan_baku }}</th>
                        <th>{{ $data->nama_bahan_baku }}</th>
                        <th>Rp. {{ format_uang($data->harga_beli) }}</th>
                        <th>{{ $data->stok }}</th>
                        <th><a onclick="selectItem({{ $data->kode_bahan_baku }})" class="btn btn-primary"><i class="fa fa-check-circle"></i> Pilih</a></th>
                      </tr>
                  @endforeach
               </tbody>
            </table>
         </div>
      
      </div>
   </div>
</div>