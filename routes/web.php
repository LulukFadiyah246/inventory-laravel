<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Mail\StockEmail;
use Illuminate\Support\Facades\Mail;

Route::get('/', 'HomeController@index')->name('home');


Auth::routes();


Route::group(['middleware' => ['web', 'cekuser:1' ]], function(){
   Route::get('kategori/data', 'KategoriController@listData')->name('kategori.data');
   Route::resource('kategori', 'KategoriController');

   Route::get('bahan_baku/data', 'BahanBakuController@listData')->name('bahan_baku.data');
   Route::post('bahan_baku/hapus', 'BahanBakuController@deleteSelected');
   Route::resource('bahan_baku', 'BahanBakuController');
   Route::get('bahan_baku/lbahanbaku', 'BahanBakuController@show');

   Route::get('supplier/data', 'SupplierController@listData')->name('supplier.data');
   Route::resource('supplier', 'SupplierController');

   Route::get('karyawan/data', 'KaryawanController@listData')->name('karyawan.data');
   Route::resource('karyawan', 'KaryawanController');

   Route::get('jenis_pengeluaran/data', 'JenisPengeluaranController@listData')->name('jenis_pengeluaran.data');
   Route::resource('jenis_pengeluaran', 'JenisPengeluaranController');

   Route::get('pembelian/data', 'PembelianController@listData')->name('pembelian.data');
   Route::get('pembelian/{id}/tambah', 'PembelianController@create');
   Route::get('pembelian/{id}/lihat', 'PembelianController@show');
   Route::resource('pembelian', 'PembelianController');

   Route::get('dpembelian/{id}/data', 'DPembelianController@listData')->name('dpembelian.data');
   Route::get('dpembelian/loadform/{diskon}/{total}', 'DPembelianController@loadForm');
   Route::resource('dpembelian', 'DPembelianController'); 

   Route::get('pengeluaran/data', 'PengeluaranController@listData')->name('pengeluaran.data');
   Route::get('pengeluaran/{id}/tambah', 'PengeluaranController@create');
   Route::get('pengeluaran/{id}/lihat', 'PengeluaranController@show');
   Route::resource('pengeluaran', 'PengeluaranController');

   Route::get('dpengeluaran/{id}/data', 'DPengeluaranController@listData')->name('dpengeluaran.data');
   Route::get('dpengeluaran/loadform/{total}', 'DPengeluaranController@loadForm');
   Route::resource('dpengeluaran', 'DPengeluaranController'); 

   Route::get('pemakaian/data', 'PemakaianController@listData')->name('pemakaian.data');
   Route::get('pemakaian/{id}/tambah', 'PemakaianController@create');
   Route::get('pemakaian/{id}/lihat', 'PemakaianController@show');
   Route::resource('pemakaian', 'PemakaianController');

   Route::get('dpemakaian/{id}/data', 'DPemakaianController@listData')->name('dpemakaian.data');
   Route::get('dpemakaian/loadform/{total}', 'DPemakaianController@loadForm');
   Route::resource('dpemakaian', 'DPemakaianController'); 

   Route::get('laporan', 'LaporanController@index')->name('laporan.index');
   Route::post('laporan', 'LaporanController@refresh')->name('laporan.refresh');
   Route::get('laporan/data/{awal}/{akhir}', 'LaporanController@listData')->name('laporan.data'); 
   Route::get('laporan/pdf/{awal}/{akhir}', 'LaporanController@exportPDF');

   Route::get('lpembelian', 'LPembelianController@index')->name('lpembelian.index');
   Route::post('lpembelian', 'LPembelianController@refresh')->name('lpembelian.refresh');
   Route::get('lpembelian/data/{awal}/{akhir}', 'LPembelianController@listData')->name('lpembelian.data'); 
   Route::get('lpembelian/pdf/{awal}/{akhir}', 'LPembelianController@exportPDF');

   Route::get('lpengeluaran', 'LPengeluaranController@index')->name('lpengeluaran.index');
   Route::post('lpengeluaran', 'LPengeluaranController@refresh')->name('lpengeluaran.refresh');
   Route::get('lpengeluaran/data/{awal}/{akhir}', 'LPengeluaranController@listData')->name('lpengeluaran.data'); 
   Route::get('lpengeluaran/pdf/{awal}/{akhir}', 'LPengeluaranController@exportPDF');

   Route::get('lpemakaian', 'LPemakaianController@index')->name('lpemakaian.index');
   Route::post('lpemakaian', 'LPemakaianController@refresh')->name('lpemakaian.refresh');
   Route::get('lpemakaian/data/{awal}/{akhir}', 'LPemakaianController@listData')->name('lpemakaian.data'); 
   Route::get('lpemakaian/pdf/{awal}/{akhir}', 'LPemakaianController@exportPDF');

   Route::get('user/data', 'UserController@listData')->name('user.data');
   Route::get('user/profil', 'UserController@profil')->name('user.profil');
   Route::patch('user/{id}/change', 'UserController@changeProfil');
   Route::resource('user', 'UserController');

   Route::resource('setting', 'SettingController');

   //email
   Route::get('/email', function() {
      return view('send_email');
   });

   Route::get('/kirimemail',function(){
      Mail::to('luluk.9456@students.amikom.ac.id')->send(new StockEmail());
   });

   Route::post('/sendEmail', 'Email@sendEmail');
});


Route::group(['middleware' => 'web'], function(){
   Route::get('user/profil', 'UserController@profil')->name('user.profil');
   Route::patch('user/{id}/change', 'UserController@changeProfil');

   Route::get('laporan', 'LaporanController@index')->name('laporan.index');
   Route::post('laporan', 'LaporanController@refresh')->name('laporan.refresh');
   Route::get('laporan/data/{awal}/{akhir}', 'LaporanController@listData')->name('laporan.data'); 
   Route::get('laporan/pdf/{awal}/{akhir}', 'LaporanController@exportPDF');

   Route::get('lpembelian', 'LPembelianController@index')->name('lpembelian.index');
   Route::post('lpembelian', 'LPembelianController@refresh')->name('lpembelian.refresh');
   Route::get('lpembelian/data/{awal}/{akhir}', 'LPembelianController@listData')->name('lpembelian.data'); 
   Route::get('lpembelian/pdf/{awal}/{akhir}', 'LPembelianController@exportPDF');

   Route::get('lpengeluaran', 'LPengeluaranController@index')->name('lpengeluaran.index');
   Route::post('lpengeluaran', 'LPengeluaranController@refresh')->name('lpengeluaran.refresh');
   Route::get('lpengeluaran/data/{awal}/{akhir}', 'LPengeluaranController@listData')->name('lpengeluaran.data'); 
   Route::get('lpengeluaran/pdf/{awal}/{akhir}', 'LPengeluaranController@exportPDF');

   Route::get('lpemakaian', 'LPemakaianController@index')->name('lpemakaian.index');
   Route::post('lpemakaian', 'LPemakaianController@refresh')->name('lpemakaian.refresh');
   Route::get('lpemakaian/data/{awal}/{akhir}', 'LPemakaianController@listData')->name('lpemakaian.data'); 
   Route::get('lpemakaian/pdf/{awal}/{akhir}', 'LPemakaianController@exportPDF');
});